# IntercomTest

IntercomTest is an example app using Haversine Formula to calculate the "great-circle distance" between 2 coordinates.
More about great-circle and Haversine Formula can be found [here](https://en.wikipedia.org/wiki/Great-circle_distance).

## The technical problem

We have some customer records in a text file (customers.txt) -- one customer per line, JSON lines formatted. We want to 
invite any customer within 100km of our Dublin office for some food and drinks on us. 
Write a program that will read the full list of customers and output the names and user ids of matching customers 
(within 100km), sorted by User ID (ascending).

* You must use the first formula from this Wikipedia article to calculate distance. Don't forget, you'll need to convert degrees to radians.

* The GPS coordinates for our Dublin office are 53.339428, -6.257664.

* You can find the Customer list [here](https://s3.amazonaws.com/intercom-take-home-test/customers.txt).

We're looking for you to produce working code, with enough room to demonstrate how to structure components in a small 
program. Good submissions are well composed. Calculating distances and reading from a file are separate concerns. 
Classes or functions have clearly defined responsibilities.  Poor submissions will be in the form of one big function. 
It�s impossible to test anything smaller than the entire operation of the program, including reading from the input file.

## The Solution

The Haversine Formula itself is not too complex to understand and many code examples can be easily found so taking this in
consideration as well as how testing and code structure was emphasized in the technical problem, it's clear that the our biggest concern here is
not the resolution of the problem itself but how we implement/struct the code to do so.

So here is a summary of how I implemented the solution for this challenge:

### Visual/Functional

Simple Android app containing 2 screens:

1) Input (Home screen of the app) which contains

* A simple visual representation of the file to be read (label and extension centered in a container) just as a way
to represent the file and also I had in mind to implement a functionality where user could click on this container and
be able to choose a different file as data source. Didn't implement this functionality as didn't seems necessary and viable
within given time. The file itself is located in the "raw" resource folder of the app.

* A SeekBar allowing the user to choose different distances for the customer search. I made sure to have 100km as its default
value so if user wants to just perform the search stated in the challenge description then no changes are needed on the
screen.

* A button to start the search.

2) Result (automatically selected after search is executed) which contains

* List of customers (id and name) that are within the distance specified on the Input screen.

### Architecture/Technology

I made use of

* MVVM (Model, View, ViewModel) architecture using Interactors and UseCases to apply the "clean" version of it.
* Retrofit for API calls (the test itself doesn't require any API call but I chose to add an example of how I would it
do if API calls were necessary to retrieve list of customers or to validate the result of the filter.
* AndroidX for Android support libs
* Gson library for JSON manipulation
* LiveData for View-ViewModel communication avoiding memory leaks
* JUnit for Unit Tests

### Data Result

The search for customers within range of 100Km from the Intercom Dublin office from list contained in the provided file
resulted in the following 16 customers:

* 4 - Ian Kehoe
* 5 - Nora Dempsey
* 6 - Theresa Enright
* 8 - Eoin Ahearn
* 11 - Richard Finnegan
* 12 - Christina McArdle
* 13 - Olive Ahearn
* 15 - Michael Ahearn
* 17 - Patricia Cahill
* 23 - Eoin Gallagher
* 24 - Rose Enright
* 26 - Stephen McArdle
* 29 - Oliver Ahearn
* 30 - Nick Enright
* 31 - Alan Behan
* 39 - Lisa Ahearn


## Installation

Simply open the project on Android Studio IDE and execute "app" module either on an emulator or physical Android device.

## Test Cases Execution

Right click on class `Main` contained inside `test` package and select "Run Main". The results will appear on Android
Studio console.

## App Usage

Input screen is the Home screen of the app. 

On Input screen select desired distance for the search and click on "SEARCH" buttom. The result will be automatically 
shown on the Result screen or a message will be displayed if no customers were found within selected range.


