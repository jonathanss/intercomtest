package com.jonathan.test;

import android.app.Application;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Application class
 */
public class TestApp extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
