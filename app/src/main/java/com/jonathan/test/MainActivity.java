package com.jonathan.test;


import com.jonathan.test.screens.mainhost.view.MainHostActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Launcher Activity
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startActivity(new Intent(this, MainHostActivity.class));
        finish();
    }
}
