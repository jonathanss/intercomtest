package com.jonathan.test.common.pojo;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Used by ViewModels in order to indicate View when a message should be shown to user
 */
public class MessageWrapper {
    private boolean showMessage;
    private int messageId;

    /**
     * MessageWrapper constructor
     */
    public MessageWrapper(){
        this.showMessage = false;
        this.messageId = -1;
    }

    /**
     * MessageWrapper constructor
     * @param messageId Message resource id to be shown
     */
    public MessageWrapper(int messageId){
        this.showMessage = true;
        this.messageId = messageId;
    }

    /**
     * Gets whether message needs to be shown or not
     * @return True if message needs to be shown
     */
    public boolean isShowMessage() {
        return showMessage;
    }

    /**
     * Gets message resource id
     * @return Message resource id
     */
    public int getMessageId() {
        return messageId;
    }
}
