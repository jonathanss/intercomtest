package com.jonathan.test.util;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contains all common, small function used within the app
 */
public class FunctionUtil {

    /**
     * Requests whether or not a string is empty. Note, a null, empty or containing only spaces will be considered empty
     * @param string String to be verified
     * @return True is string is null, empty or containing only spaces
     */
    public static boolean isEmpty(String string){
        return string == null || string.replace(" ", "").isEmpty();
    }
}
