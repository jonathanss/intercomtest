package com.jonathan.test.util;

import com.jonathan.test.R;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contains all common Enums used within the app
 */
public class EnumUtil {

    /**
     * intercomtest
     *
     * Created by jonathan
     * Creation date Sat 22 of Feb, 2020
     *
     * Description:
     * Used to identify response codes received from backend
     */
    public enum MXResponseCode {
        SUCCESS("MW200", -1),
        EXCEPTION_ERROR("9000", R.string.error_generic),
        NO_INTERNET_IS_AVAILABLE("9002", R.string.error_no_internet);

        public String value;
        public int message;

        /**
         * MXResponseCode constructor
         * @param value code
         * @param message equivalent message
         */
        MXResponseCode(String value, int message) {
            this.value = value;
            this.message = message;
        }
    }
}
