package com.jonathan.test.util;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contains all constants within the app
 */
public class ConstUtil {

    /**
     * intercomtest
     *
     * Created by jonathan
     * Creation date Sat 22 of Feb, 2020
     *
     * Description:
     * Constants of value definition
     */
    public static class def{
        public static final String DEFAULT_SOURCE_FILE_NAME = "customers.txt";
        public static final int DEFAULT_DISTANCE_SELECTED = 100;
        public static final int DEFAULT_DISTANCE_MINIMUM = 1;
        public static final int DEFAULT_DISTANCE_MAXIMUM = 150;

        public static final double DUBLIN_OFFICE_LAT = 53.339428;
        public static final double DUBLIN_OFFICE_LONG = -6.257664;
    }
}
