package com.jonathan.test.util;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Class containing common functions used by activities
 */
public class ActivityUtils {

    /**
     * Requests Fragment to be attached to Activity
     * @param fragmentManager Android Fragment manager
     * @param fragment Fragment to be attached
     * @param frameId Id of the frame that will hold Fragment instance
     */
    public static void attachFragmentToActivity (@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment, int frameId) {
        attachFragmentToActivity(fragmentManager, fragment, frameId, null);
    }

    /**
     * Requests Fragment to be attached to Activity
     * @param fragmentManager Android Fragment manager
     * @param fragment Fragment to be attached
     * @param frameId Id of the frame that will hold Fragment instance
     * @param tag Tag to identify Fragment on FragmentManager
     */
    public static void attachFragmentToActivity (@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment, int frameId, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment, tag);
        transaction.commit();
    }
}
