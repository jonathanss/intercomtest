package com.jonathan.test.model.entity;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Objects;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Customer entity
 */
@Parcel
public class ECustomer {

    @SerializedName("user_id")
    long id;

    @SerializedName("name")
    String name;

    @SerializedName("latitude")
    double latitude;

    @SerializedName("longitude")
    double longitude;

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }

        if(obj instanceof ECustomer){
            ECustomer customer = (ECustomer)obj;

            return customer.getId() == id &&
                    customer.getName() != null && customer.getName().equals(name);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, latitude, longitude);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
