package com.jonathan.test.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.parceler.Parcel;


/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Response example in case a validation of customer list is needed on backend in future implementations
 */
@Parcel
public class TestResponseConfirmation extends TestResponse {

    @JsonProperty("success")
    boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
