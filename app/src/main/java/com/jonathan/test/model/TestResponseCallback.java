package com.jonathan.test.model;


import com.jonathan.test.model.response.TestResponse;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Interface contract for all callback classes
 */
public interface TestResponseCallback<RESPONSE extends TestResponse> {

    void onSuccess(RESPONSE response);

    void onError(RESPONSE response);

    void onError(String errorCode);
}
