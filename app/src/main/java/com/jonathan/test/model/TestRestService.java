package com.jonathan.test.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jonathan.test.model.response.TestResponse;
import com.jonathan.test.util.EnumUtil;

import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Rest Service basic class containing all common code shared between rest services classes within the app
 */
public class TestRestService {
    private static final String BASE_URL = "";
    private static Retrofit retrofit;
    protected static String apiKey = "";

    /**
     * Requests instance of Retrofit
     * @return Retrofit singleton instance
     */
    protected static Retrofit getRetrofit(){
        if(retrofit == null){
            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            builder.readTimeout(1, TimeUnit.MINUTES);
            builder.connectTimeout(1, TimeUnit.MINUTES);

            OkHttpClient client = builder.build();

            DateFormat dateFormat = new SimpleDateFormat("yyyy'-'MM'-'dd", Locale.ENGLISH);
            ObjectMapper mapper = new ObjectMapper();
            mapper.setDateFormat(dateFormat);
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(JacksonConverterFactory.create(mapper))
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    /**
     * Responsible for making the backend call
     * @param call Call to be enqueued
     * @param callback Callback implementation for response handling
     */
    protected <RESPONSE extends TestResponse> void callEndPoint(@NonNull Call<RESPONSE> call, @NonNull TestResponseCallback<RESPONSE> callback){
        call.enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(@NonNull Call<RESPONSE> call, @NonNull Response<RESPONSE> response) {
                if(isSuccess(response)) {
                    callback.onSuccess(response.body());

                }else if(response.body() != null){
                    callback.onError(response.body());

                }else {
                    callback.onError(EnumUtil.MXResponseCode.EXCEPTION_ERROR.value);
                }
            }

            @Override
            public void onFailure(@NonNull Call<RESPONSE> call, @NonNull Throwable throwable) {
                callback.onError(EnumUtil.MXResponseCode.EXCEPTION_ERROR.value);
            }
        });
    }

    /**
     * Handle service call exceptions
     * @param error Exception to be handled
     * @param response Received response
     * @param callback Callback responsible for this response handling
     */
    protected <RESPONSE extends TestResponse> void handleServiceCallException(Exception error, @NonNull RESPONSE response, TestResponseCallback<RESPONSE> callback){
        if(error.getMessage() != null && error.getMessage().equals(EnumUtil.MXResponseCode.NO_INTERNET_IS_AVAILABLE.value)
                || error instanceof UnknownHostException){
            if(callback != null) {
                callback.onError(EnumUtil.MXResponseCode.NO_INTERNET_IS_AVAILABLE.value);
            }
        }else{
            response.setCode(EnumUtil.MXResponseCode.EXCEPTION_ERROR.value);
            response.setMessage(error.getMessage());

            if(callback != null) {
                callback.onError(response);
            }
        }
    }

    /**
     * Identifies if response is a successful response or not
     * @param response Response to be verified
     * @return Whether response is successful
     */
    private boolean isSuccess(Response<? extends TestResponse> response){
        return response != null && response.code() == 200;
    }
}
