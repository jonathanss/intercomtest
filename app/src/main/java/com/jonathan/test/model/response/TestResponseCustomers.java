package com.jonathan.test.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jonathan.test.model.entity.ECustomer;

import org.parceler.Parcel;

import java.util.List;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Response example in case of customer list being retrieved from backend in future implementations
 */
@Parcel
public class TestResponseCustomers extends TestResponse {

    @JsonProperty("customers")
    List<ECustomer> customers;

    public List<ECustomer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<ECustomer> customers) {
        this.customers = customers;
    }
}
