package com.jonathan.test.model.service;

import com.jonathan.test.model.request.TestRequestValidateCustomerList;
import com.jonathan.test.model.response.TestResponseCustomers;
import com.jonathan.test.model.response.TestResponseConfirmation;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Maps all endpoints related to customers
 */
public interface CustomerEndPoints {

    @GET("test/v3/customers/retrieve")
    Call<TestResponseCustomers> retrieveCustomers(@Query("userKey") String userKey);

    @POST("test/v3/customers/validate")
    Call<TestResponseConfirmation> validateCustomers(@Query("userKey") String userKey, @Body TestRequestValidateCustomerList request);
}
