package com.jonathan.test.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jonathan.test.model.entity.ECustomer;

import org.parceler.Parcel;

import java.util.List;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Request example in case a validation of customer list is needed on backend in future implementations
 */
@Parcel
public class TestRequestValidateCustomerList {

    @JsonProperty("customers")
    private List<ECustomer> customers;

    public List<ECustomer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<ECustomer> customers) {
        this.customers = customers;
    }
}
