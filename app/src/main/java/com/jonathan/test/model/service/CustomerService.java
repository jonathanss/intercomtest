package com.jonathan.test.model.service;

import com.jonathan.test.model.TestResponseCallback;
import com.jonathan.test.model.TestRestService;
import com.jonathan.test.model.request.TestRequestValidateCustomerList;
import com.jonathan.test.model.response.TestResponseConfirmation;
import com.jonathan.test.model.response.TestResponseCustomers;

import retrofit2.Call;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contains API calls for all customer related APIs
 */
public class CustomerService extends TestRestService {

    /**
     * Request list of customers
     * @param callback Callback for response handling
     */
    public void retrieveCustomers(TestResponseCallback<TestResponseCustomers> callback) {
        try {
            Call<TestResponseCustomers> call  = getRetrofit().create(CustomerEndPoints.class).retrieveCustomers(apiKey);
            callEndPoint(call, callback);

        }catch (Exception error){
            handleServiceCallException(error, new TestResponseCustomers(), callback);
        }
    }

    /**
     * Request list of filtered customers to be validated on backend
     * @param request Request to be sent
     * @param callback Callback for response handling
     */
    public void validateCustomerList(TestRequestValidateCustomerList request, TestResponseCallback<TestResponseConfirmation> callback) {
        try {
            Call<TestResponseConfirmation> call  = getRetrofit().create(CustomerEndPoints.class).validateCustomers(apiKey, request);
            callEndPoint(call, callback);

        }catch (Exception error){
            handleServiceCallException(error, new TestResponseConfirmation(), callback);
        }
    }
}