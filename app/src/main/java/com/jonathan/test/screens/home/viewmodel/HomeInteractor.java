package com.jonathan.test.screens.home.viewmodel;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.home.HomeContract;
import com.jonathan.test.util.ConstUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Interactor used by HomeViewModel containing all business logic and data manipulation needed by HomeViewModel
 */
public class HomeInteractor implements HomeContract.Interactor {

    @Override
    public List<ECustomer> parse(List<JsonObject> jsonCustomers) {
        List<ECustomer> result = new ArrayList<>();

        if(jsonCustomers != null && !jsonCustomers.isEmpty()){
            Gson gson = new GsonBuilder().create();

            for(JsonObject jsonObject : jsonCustomers){
                ECustomer customer = gson.fromJson(jsonObject, ECustomer.class);

                if(isValidCustomer(customer)){
                    result.add(customer);
                }
            }
        }

        return result;
    }

    @Override
    public List<ECustomer> filter(List<ECustomer> customers, int distanceInKilometers) {
        List<ECustomer> result = new ArrayList<>();

        if(customers != null && !customers.isEmpty()){
            double officeLatRadians = Math.toRadians(ConstUtil.def.DUBLIN_OFFICE_LAT);
            double officeLongRadians = Math.toRadians(ConstUtil.def.DUBLIN_OFFICE_LONG);

            for (ECustomer customer : customers){
                if(isValidCustomer(customer)){
                    double customerLatRadians = Math.toRadians(customer.getLatitude());
                    double customerLongRadians = Math.toRadians(customer.getLongitude());

                    double kmDistance = calculateHaversineDistance(officeLatRadians, officeLongRadians, customerLatRadians, customerLongRadians);

                    if(kmDistance <= distanceInKilometers){
                        result.add(customer);
                    }
                }
            }
        }

        return result;
    }

    /**
     * Requests whether customer is a valid customer to be listed
     * @param customer customer to be checked
     * @return True if customer is not null, has ID, latitude and longitude greater than 0 and has name.
     */
    private boolean isValidCustomer(ECustomer customer){
        return customer != null && customer.getId() > 0 && customer.getName() != null && customer.getLatitude() != 0 && customer.getLongitude() != 0;
    }

    /**
     * Calculate the distance of 2 points using Formula of Haversine.
     * Formula explained:
     *
     * distance = R * 2 * angle which sine = (√ of (sine²(ΔΦ/2) + cosine of Φ1 * cosine of Φ2 * sine²(Δλ/2))
     * being:
     *
     * R = Radius of the earth = 6371Km
     * Φ1 = Origin latitude
     * Φ2 = Destination latitude
     * λ1 = Origin longitude
     * λ2 = Destination longitude
     * ΔΦ = Absolute distance between Φ1 and Φ2
     * Δλ = Absolute distance between λ1 and λ2
     *
     * @param originLatRadians Latitude of origin point (in radians)
     * @param originLongRadians Longitude of origin point (in radians)
     * @param destinationLatRadians Latitude of destination point (in radians)
     * @param destinationLongRadians Longitude of destination point (in radians)
     *
     * @return distance in kilometers between the 2 points using Formula of Haversine
     */
    public double calculateHaversineDistance(double originLatRadians, double originLongRadians, double destinationLatRadians, double destinationLongRadians){
        // R
        double earthRadius = 6371.0;
        // ΔΦ
        double absoluteLatDistance = destinationLatRadians - originLatRadians;
        // Δλ
        double absoluteLongDistance = destinationLongRadians - originLongRadians;
        // sine²(ΔΦ/2)
        double sineSquareAbsoluteLat = havSin(absoluteLatDistance);
        // sine²(Δλ/2)
        double sineSquareAbsoluteLong = havSin(absoluteLongDistance);
        // sine²(ΔΦ/2) + cosΦ1 * cosΦ2 * sine²(Δλ/2)
        double angle = sineSquareAbsoluteLat + Math.cos(originLatRadians) * Math.cos(destinationLatRadians) * sineSquareAbsoluteLong;
        // 2 * angle which sine = (√ of (sine²(ΔΦ/2) + cosine of Φ1 * cosine of Φ2 * sine²(Δλ/2))
        double centralAngle = 2 * Math.asin(Math.sqrt(angle));

        // R * 2 * angle which sine = (√ of (sine²(ΔΦ/2) + cosine of Φ1 * cosine of Φ2 * sine²(Δλ/2))
        return earthRadius * centralAngle;
    }

    /**
     * HaverSin calculation can be simplified by sine²(value/2)
     * @param value value to be used
     * @return hav of value
     */
    private double havSin(double value){
        return Math.pow(Math.sin(value / 2), 2);
    }
}
