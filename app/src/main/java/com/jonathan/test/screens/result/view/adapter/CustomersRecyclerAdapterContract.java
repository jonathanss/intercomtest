package com.jonathan.test.screens.result.view.adapter;

import com.jonathan.test.model.entity.ECustomer;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contract interface use of CustomerRecyclerAdapter
 */
public interface CustomersRecyclerAdapterContract {

    /**
     * intercomtest
     *
     * Created by jonathan
     * Creation date Sat 22 of Feb, 2020
     *
     * Description:
     * Contract interface ViewModel used by CustomerRecyclerAdapter
     */
    interface ViewModel{

        void bindViewHolder(int position, ECustomer item, ViewHolder holder);
    }

    /**
     * intercomtest
     *
     * Created by jonathan
     * Creation date Sat 22 of Feb, 2020
     *
     * Description:
     * Contract interface ViewHolder used by CustomerRecyclerAdapter
     */
    interface ViewHolder{

        void setViewTags(ECustomer customer);

        void setCustomerId(String id);

        void setCustomerName(String name);
    }
}
