package com.jonathan.test.screens.home.viewmodel;

import com.jonathan.test.R;
import com.jonathan.test.common.pojo.MessageWrapper;
import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.BaseViewModel;
import com.jonathan.test.screens.home.HomeContract;
import com.jonathan.test.usecase.RetrieveCustomersUseCase;
import com.jonathan.test.usecase.exception.ReadFileException;
import com.jonathan.test.util.ConstUtil;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Home ViewModel implementation
 */
public class HomeViewModel extends BaseViewModel implements HomeContract.ViewModel {
    private HomeContract.Interactor mInteractor;

    private MutableLiveData<List<ECustomer>> filteredCustomers;
    private MutableLiveData<Integer> defaultSelectedDistance;
    private MutableLiveData<Integer> defaultMinimumDistance;
    private MutableLiveData<Integer> defaultMaximumDistance;
    private MutableLiveData<String> sourceFileName;
    private MutableLiveData<Boolean> searchButtonEnabled;

    /**
     * HomeViewModel constructor
     * @param interactor Interactor required by HomeViewModel
     */
    public HomeViewModel(HomeContract.Interactor interactor){
        mInteractor = interactor;

        filteredCustomers = new MutableLiveData<>();
        defaultSelectedDistance = new MutableLiveData<>();
        defaultMinimumDistance = new MutableLiveData<>();
        defaultMaximumDistance = new MutableLiveData<>();
        sourceFileName = new MutableLiveData<>();
        searchButtonEnabled = new MutableLiveData<>();

        sourceFileName.setValue(ConstUtil.def.DEFAULT_SOURCE_FILE_NAME);
        defaultMinimumDistance.setValue(ConstUtil.def.DEFAULT_DISTANCE_MINIMUM);
        defaultMaximumDistance.setValue(ConstUtil.def.DEFAULT_DISTANCE_MAXIMUM);
        defaultSelectedDistance.postValue(ConstUtil.def.DEFAULT_DISTANCE_SELECTED);

        searchButtonEnabled.setValue(true);
    }


    /******************** VIEW MODEL CONTRACT ********************/

    @Override
    public void handleSearchButtonClick(int distance) {
        searchButtonEnabled.setValue(false);
        RetrieveCustomersUseCase retrieveCustomersUseCase = new RetrieveCustomersUseCase();

        try {
            List<ECustomer> customers = mInteractor.parse(retrieveCustomersUseCase.readFromInternalFile());
            List<ECustomer> resultCustomers = mInteractor.filter(customers, distance);

            if(resultCustomers.isEmpty()){
                message.setValue(new MessageWrapper(R.string.home_message_customers_not_found));

            }else{
                this.filteredCustomers.setValue(resultCustomers);
            }
        } catch (ReadFileException error) {
            error.printStackTrace();
            message.setValue(new MessageWrapper(error.getUserFriendlyMessageId()));

        } catch (Exception error){
            error.printStackTrace();
            //Crashlytics
        }

        searchButtonEnabled.setValue(true);
    }

    @Override
    public LiveData<List<ECustomer>> getFilteredCustomers() {
        return filteredCustomers;
    }

    @Override
    public LiveData<Integer> getDefaultSelectedDistance() {
        return defaultSelectedDistance;
    }

    @Override
    public LiveData<Integer> getDefaultMinimumDistance() {
        return defaultMinimumDistance;
    }

    @Override
    public LiveData<Integer> getDefaultMaximumDistance() {
        return defaultMaximumDistance;
    }

    @Override
    public LiveData<String> getSourceFileName() {
        return sourceFileName;
    }

    @Override
    public LiveData<Boolean> getSearchButtonStatus() {
        return searchButtonEnabled;
    }
}
