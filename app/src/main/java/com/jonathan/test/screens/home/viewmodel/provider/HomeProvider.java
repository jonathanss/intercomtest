package com.jonathan.test.screens.home.viewmodel.provider;

import com.jonathan.test.screens.home.viewmodel.HomeInteractor;
import com.jonathan.test.screens.home.viewmodel.HomeViewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * ViewModel provider for HomeViewModel
 */
public class HomeProvider {

    /**
     * This design allows the uso of the same ViewModel by different Views
     */
    public class Home extends ViewModelProvider.NewInstanceFactory{

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if(modelClass.isAssignableFrom(HomeViewModel.class)){
                return (T) new HomeViewModel(new HomeInteractor());
            }
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
