package com.jonathan.test.screens;

import com.google.android.material.snackbar.Snackbar;

import com.jonathan.test.R;
import com.jonathan.test.TestApp;
import com.jonathan.test.common.pojo.MessageWrapper;
import com.jonathan.test.util.FunctionUtil;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Fragment base containing common code shared between Fragments within the app
 */
public abstract class BaseFragment<VIEW_MODEL extends BaseContract.ViewModel, HOST extends BaseContract.Host> extends Fragment {
    protected VIEW_MODEL mViewModel;
    protected HOST mHost;
    protected ViewModelProvider.NewInstanceFactory mFactory;
    protected String viewModelKey;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();

        if(mViewModel != null){
            mViewModel.messages().observe(this, this::onShowMessageRequested);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof BaseContract.Host) {
            mHost = (HOST) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Host");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHost = null;
    }

    /**
     * Request ViewModel to be initialized
     */
    protected abstract void initViewModel();

    /**
     * Requests ViewModel factory to be defined
     * @param factory Factory to be used
     * @param viewModelKey Key to identify ViewModel instance
     */
    public void defineViewModelFactory(ViewModelProvider.NewInstanceFactory factory, String viewModelKey){
        this.mFactory = factory;
        this.viewModelKey = viewModelKey;
    }

    /**
     * Requests message to be shown in form of snakebar
     * @param message Message to be shown
     */
    private void onShowMessageRequested(MessageWrapper message) {
        if(message != null && message.isShowMessage() && getActivity() != null){
            Snackbar.make(getActivity().findViewById(R.id.main_layout), getString(message.getMessageId()), Snackbar.LENGTH_LONG).show();
        }
    }
}
