package com.jonathan.test.screens;

import com.jonathan.test.common.pojo.MessageWrapper;

import androidx.lifecycle.LiveData;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contract base interface for all screen contracts within the app
 */
public interface BaseContract {

    /**
     * Base ViewModel contract
     */
    interface ViewModel {

        /**
         * Gets message wrapper for message observation
         * @return Live data of message wrapper
         */
        LiveData<MessageWrapper> messages();
    }

    /**
     * Base Host contract
     */
    interface Host {

        /**
         * Request current Activity to be closed
         */
        void closeScreen();
    }
}
