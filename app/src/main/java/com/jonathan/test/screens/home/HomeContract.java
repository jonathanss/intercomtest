package com.jonathan.test.screens.home;


import com.google.gson.JsonObject;

import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.BaseContract;

import java.util.List;

import androidx.lifecycle.LiveData;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contract interface for Home Screen
 */
public interface HomeContract {

    /**
     * ViewModel contract used by Home View
     */
    interface ViewModel extends BaseContract.ViewModel {

        /**
         * Requests filtered customers live data for its observation
         * @return filtered customers live data for its observation
         */
        LiveData<List<ECustomer>> getFilteredCustomers();

        /**
         * Requests default selected distance live data for its observation
         * @return default selected distance live data for its observation
         */
        LiveData<Integer> getDefaultSelectedDistance();

        /**
         * Requests default minimum distance live data for its observation
         * @return default minimum distance live data for its observation
         */
        LiveData<Integer> getDefaultMinimumDistance();

        /**
         * Requests default maximum distance live data for its observation
         * @return default maximum distance live data for its observation
         */
        LiveData<Integer> getDefaultMaximumDistance();

        /**
         * Requests source file name live data for its observation
         * @return source file name live data for its observation
         */
        LiveData<String> getSourceFileName();

        /**
         * Requests search button status live data for its observation
         * @return search button status live data for its observation
         */
        LiveData<Boolean> getSearchButtonStatus();

        /**
         * Requests search button click event to be handled
         */
        void handleSearchButtonClick(int distance);
    }

    /**
     * Interactor contract used by Home ViewModel
     */
    interface Interactor {

        /**
         * Requests list of JSON customers to be parsed
         * @param jsonCustomers List to be parsed
         * @return Parsed list of customers
         */
        List<ECustomer> parse(List<JsonObject> jsonCustomers);

        /**
         * Request list of customers to be filtered by distance
         * @param customers Full list of customers
         * @param distanceInKilometers Distance in kilometers
         * @return List of customers that are within range specified by distance parameter
         */
        List<ECustomer> filter(List<ECustomer> customers, int distanceInKilometers);
    }

    /**
     * Host contract used by Fragment to communicate with host Activity
     */
    interface Host extends BaseContract.Host {

        /**
         * Request bottom navigation bar icon to be selected
         * @param menuItemId Id of the icon to be selected
         */
        void selectNavigationBarIcon(int menuItemId);

        /**
         * Requests filtered list of customers to be shown
         * @param customers List of customers to be shown
         */
        void showFilteredCustomers(List<ECustomer> customers);
    }
}
