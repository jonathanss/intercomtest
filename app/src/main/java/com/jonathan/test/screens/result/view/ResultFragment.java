package com.jonathan.test.screens.result.view;

import com.jonathan.test.R;
import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.BaseFragment;
import com.jonathan.test.screens.result.ResultContract;
import com.jonathan.test.screens.result.view.adapter.CustomersRecyclerAdapter;
import com.jonathan.test.screens.result.viewmodel.ResultViewModel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Fragment view implementation of Result Screen
 */
public class ResultFragment extends BaseFragment<ResultContract.ViewModel, ResultContract.Host>{
    private Toolbar resultToolbar;

    private TextView txtSearchTime;
    private TextView txtNoCustomers;
    private CustomersRecyclerAdapter customersAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel.getResultCustomers().observe(this, this::onResultCustomersChanged);
        mViewModel.getLabelNoCustomersVisibility().observe(this, this::onLabelNoCustomersVisibilityChanged);
        mViewModel.getLabelResultTimeVisibility().observe(this, this::onLabelResultTimeVisibilityChanged);
        mViewModel.getResultTime().observe(this, this::onResultTimeChanged);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_result, container, false);

        resultToolbar = view.findViewById(R.id.result_fragment_toolbar);

        ((AppCompatActivity)requireActivity()).setSupportActionBar(resultToolbar);
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        RecyclerView customersRecycleView = view.findViewById(R.id.result_fragment_recycler_view);
        txtSearchTime = view.findViewById(R.id.result_fragment_last_search_time);
        txtNoCustomers = view.findViewById(R.id.result_fragment_label_no_result);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireActivity()) {
            @Override
            public boolean supportsPredictiveItemAnimations() {
                return false;
            }
        };
        customersRecycleView.setLayoutManager(linearLayoutManager);

        customersAdapter = new CustomersRecyclerAdapter(getContext(), mViewModel);
        customersRecycleView.setAdapter(customersAdapter);

        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if(!hidden && mHost != null){
            mHost.selectNavigationBarIcon(R.id.menu_item_result);

            ((AppCompatActivity)requireActivity()).setSupportActionBar(resultToolbar);
            ((AppCompatActivity)requireActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    protected void initViewModel() {
        mViewModel = ViewModelProviders.of(this, mFactory).get(viewModelKey, ResultViewModel.class);
    }

    /**
     * Indicates a new search was made and a new result set is available
     * @param customers result of last search
     */
    public void onNewSearchPerformed(List<ECustomer> customers){
        mViewModel.onNewSearchPerformed(customers);
    }


    /******************** LIVE DATA OBSERVERS ********************/

    private void onResultCustomersChanged(List<ECustomer> customers){
        customersAdapter.setData(customers);
    }

    private void onLabelNoCustomersVisibilityChanged(boolean visible){
        txtNoCustomers.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void onLabelResultTimeVisibilityChanged(boolean visible){
        txtSearchTime.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void onResultTimeChanged(String time){
        txtSearchTime.setText(getString(R.string.result_label_search_time, time));
    }
}
