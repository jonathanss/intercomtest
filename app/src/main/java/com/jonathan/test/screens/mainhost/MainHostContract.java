package com.jonathan.test.screens.mainhost;

import com.jonathan.test.screens.BaseContract;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contract interface for Main Host Activity
 */
public interface MainHostContract {

    /**
     * ViewModel contract used by Main Host View
     */
    interface ViewModel extends BaseContract.ViewModel {

    }
}
