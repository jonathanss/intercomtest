package com.jonathan.test.screens.result.viewmodel.provider;

import com.jonathan.test.screens.result.viewmodel.ResultInteractor;
import com.jonathan.test.screens.result.viewmodel.ResultViewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * ViewModel provider for ResultViewModel
 */
public class ResultProvider {

    /**
     * This design allows the uso of the same ViewModel by different Views
     */
    public class Result extends ViewModelProvider.NewInstanceFactory{

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if(modelClass.isAssignableFrom(ResultViewModel.class)){
                return (T) new ResultViewModel(new ResultInteractor());
            }
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
