package com.jonathan.test.screens.result.viewmodel;

import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.result.ResultContract;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Interactor used by ResultViewModel containing all business logic and data manipulation needed by ResultViewModel
 */
public class ResultInteractor implements ResultContract.Interactor {

    @Override
    public void sortCustomers(List<ECustomer> customers) {
        if(customers != null && !customers.isEmpty()){
            Collections.sort(customers, (customer1, customer2) -> {
                if(customer1.getId() < customer2.getId()){
                    return -1;
                }else if(customer1.getId() > customer2.getId()){
                    return 1;
                }

                return  0;
            });
        }
    }

    @Override
    public String getUserFriendlyDate(Date date) {
        if(date == null){
            date = Calendar.getInstance().getTime();
        }

        return new SimpleDateFormat("E dd MMM, HH:mm", Locale.ENGLISH).format(date);
    }
}
