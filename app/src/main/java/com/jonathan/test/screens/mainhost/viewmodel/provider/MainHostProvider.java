package com.jonathan.test.screens.mainhost.viewmodel.provider;

import com.jonathan.test.screens.mainhost.viewmodel.MainHostViewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * ViewModel provider for MainHostViewModel
 */
public class MainHostProvider {

    /**
     * This design allows the uso of the same ViewModel by different Views
     */
    public class MainHost extends ViewModelProvider.NewInstanceFactory{

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if(modelClass.isAssignableFrom(MainHostViewModel.class)){
                return (T) new MainHostViewModel();
            }
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
