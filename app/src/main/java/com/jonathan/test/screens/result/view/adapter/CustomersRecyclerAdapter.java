package com.jonathan.test.screens.result.view.adapter;

import com.jonathan.test.R;
import com.jonathan.test.model.entity.ECustomer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Recycler Adapter for list of customers
 */
public class CustomersRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private CustomersRecyclerAdapterContract.ViewModel mViewModel;
    private List<ECustomer> customers;

    /**
     * CustomersRecyclerAdapter constructor
     * @param context Android context which this instance of CustomersRecyclerAdapter is being used
     * @param viewModel ViewModel implementation to be used by this adapter
     */
    public CustomersRecyclerAdapter(Context context, CustomersRecyclerAdapterContract.ViewModel viewModel){
        this.mContext = context;
        this.mViewModel = viewModel;
        this.customers = new ArrayList<>();
    }

    /**
     * Request new list of customers to be set
     * @param data Updated list of customers to be shown
     */
    public void setData(List<ECustomer> data){
        this.customers = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_customer, parent, false);
        return new CustomerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        mViewModel.bindViewHolder(position, customers.get(position), (CustomerViewHolder)holder);
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }


    /**
     * intercomtest
     *
     * Created by jonathan
     * Creation date Sat 22 of Feb, 2020
     *
     * Description:
     * ViewHolder used by this adapter
     */
    protected class CustomerViewHolder extends RecyclerView.ViewHolder implements CustomersRecyclerAdapterContract.ViewHolder {
        TextView customerId;
        TextView customerName;
        RelativeLayout layout;

        /**
         * CustomerViewHolder constructor
         * @param itemView View to be used to initiate this view holder
         */
        CustomerViewHolder(@NonNull View itemView) {
            super(itemView);

            customerId = itemView.findViewById(R.id.item_customer_id);
            customerName = itemView.findViewById(R.id.item_customer_name);
            layout = itemView.findViewById(R.id.item_customer_layout);
        }

        @Override
        public void setViewTags(ECustomer customer) {
            layout.setTag(customer);
        }

        @Override
        public void setCustomerId(String id) {
            customerId.setText(id);
        }

        @Override
        public void setCustomerName(String name) {
            customerName.setText(name);
        }
    }
}
