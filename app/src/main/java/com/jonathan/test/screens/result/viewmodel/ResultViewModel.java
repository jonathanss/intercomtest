package com.jonathan.test.screens.result.viewmodel;

import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.BaseViewModel;
import com.jonathan.test.screens.result.ResultContract;
import com.jonathan.test.screens.result.view.adapter.CustomersRecyclerAdapterContract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Result ViewModel implementation
 */
public class ResultViewModel extends BaseViewModel implements ResultContract.ViewModel {
    private ResultContract.Interactor mInteractor;

    private MutableLiveData<List<ECustomer>> resultCustomers;
    private MutableLiveData<String> resultTime;
    private MutableLiveData<Boolean> labelNoCustomersVisibility;
    private MutableLiveData<Boolean> labelResultTimeVisibility;

    /**
     * ResultViewModel constructor
     * @param interactor Interactor required by ResultViewModel
     */
    public ResultViewModel(ResultContract.Interactor interactor){
        mInteractor = interactor;

        resultCustomers = new MutableLiveData<>();
        resultTime = new MutableLiveData<>();
        labelNoCustomersVisibility = new MutableLiveData<>();
        labelResultTimeVisibility = new MutableLiveData<>();

        labelNoCustomersVisibility.setValue(true);
        labelResultTimeVisibility.setValue(false);
    }



    /******************** VIEW MODEL ADAPTER CONTRACT ********************/

    @Override
    public void bindViewHolder(int position, ECustomer customer, CustomersRecyclerAdapterContract.ViewHolder holder) {
        holder.setViewTags(customer);
        holder.setCustomerId(String.valueOf(customer.getId()));
        holder.setCustomerName(customer.getName());
    }




    /******************** VIEW MODEL CONTRACT ********************/

    @Override
    public void onNewSearchPerformed(List<ECustomer> customers) {
        resultTime.setValue(mInteractor.getUserFriendlyDate(Calendar.getInstance().getTime()));
        labelResultTimeVisibility.setValue(true);

        if(customers == null || customers.isEmpty()){
            resultCustomers.setValue(new ArrayList<>());
            labelNoCustomersVisibility.setValue(true);

        }else {
            labelNoCustomersVisibility.setValue(false);
            mInteractor.sortCustomers(customers);
            resultCustomers.setValue(customers);
        }
    }

    @Override
    public LiveData<List<ECustomer>> getResultCustomers() {
        return resultCustomers;
    }

    @Override
    public LiveData<String> getResultTime() {
        return resultTime;
    }

    @Override
    public LiveData<Boolean> getLabelNoCustomersVisibility() {
        return labelNoCustomersVisibility;
    }

    @Override
    public LiveData<Boolean> getLabelResultTimeVisibility() {
        return labelResultTimeVisibility;
    }
}
