package com.jonathan.test.screens;

import com.google.android.material.snackbar.Snackbar;

import com.jonathan.test.R;
import com.jonathan.test.common.pojo.MessageWrapper;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Activity base containing common code shared between Activities within the app
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected Context myContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /**
     * Requests all activity components to be initialized
     */
    public abstract void startComponents();

    /**
     * Requests message to be shown in form of snakebar
     * @param messageWrapper Message to be shown
     */
    protected void onShowMessageRequested(MessageWrapper messageWrapper) {
        if(messageWrapper.isShowMessage()){
            Snackbar.make(findViewById(R.id.main_layout), getString(messageWrapper.getMessageId()), Snackbar.LENGTH_LONG).show();
        }
    }
}
