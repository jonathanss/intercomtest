package com.jonathan.test.screens;

import com.jonathan.test.common.pojo.MessageWrapper;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * ViewModel base containing common code shared between ViewModels within the app
 */
public abstract class BaseViewModel extends ViewModel implements BaseContract.ViewModel {
    protected MutableLiveData<MessageWrapper> message;

    /**
     * BaseViewModel constructor
     */
    protected BaseViewModel(){
        message = new MutableLiveData<>();
    }

    @Override
    public LiveData<MessageWrapper> messages() {
        return message;
    }
}
