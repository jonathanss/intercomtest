package com.jonathan.test.screens.result;


import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.BaseContract;
import com.jonathan.test.screens.result.view.adapter.CustomersRecyclerAdapterContract;

import java.util.Date;
import java.util.List;

import androidx.lifecycle.LiveData;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Contract interface for Result Screen
 */
public interface ResultContract {

    /**
     * ViewModel contract used by Result View
     */
    interface ViewModel extends BaseContract.ViewModel, CustomersRecyclerAdapterContract.ViewModel {

        /**
         * Requests result customers live data for its observation
         * @return result customers live data for its observation
         */
        LiveData<List<ECustomer>> getResultCustomers();

        /**
         * Requests result time live data for its observation
         * @return result time live data for its observation
         */
        LiveData<String> getResultTime();

        /**
         * Requests label no customers visibility live data for its observation
         * @return label no customers visibility live data for its observation
         */
        LiveData<Boolean> getLabelNoCustomersVisibility();

        /**
         * Requests label result time visibility live data for its observation
         * @return label result time visibility live data for its observation
         */
        LiveData<Boolean> getLabelResultTimeVisibility();

        /**
         * Request the list of customers resulted from a new search to be handled
         * @param customers list of customers resulted from a new search
         */
        void onNewSearchPerformed(List<ECustomer> customers);
    }

    /**
     * Interactor contract used by Result ViewModel
     */
    interface Interactor {

        /**
         * Requests list of customers to be sorted by their IDs
         * @param customers list of customers to be sorted
         */
        void sortCustomers(List<ECustomer> customers);

        /**
         * Requests user friendly date representation
         * @param date Date to be friendly represented
         * @return user friendly date representation
         */
        String getUserFriendlyDate(Date date);
    }

    /**
     * Host contract used by Fragment to communicate with host Activity
     */
    interface Host extends BaseContract.Host {

        /**
         * Request bottom navigation bar icon to be selected
         * @param menuItemId Id of the icon to be selected
         */
        void selectNavigationBarIcon(int menuItemId);
    }
}
