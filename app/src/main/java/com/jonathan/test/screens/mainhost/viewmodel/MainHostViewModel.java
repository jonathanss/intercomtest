package com.jonathan.test.screens.mainhost.viewmodel;

import com.jonathan.test.screens.BaseViewModel;
import com.jonathan.test.screens.mainhost.MainHostContract;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Home ViewModel implementation
 */
public class MainHostViewModel extends BaseViewModel implements MainHostContract.ViewModel {

}
