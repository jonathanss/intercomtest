package com.jonathan.test.screens.mainhost.view;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jonathan.test.R;
import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.BaseActivity;
import com.jonathan.test.screens.home.HomeContract;
import com.jonathan.test.screens.home.view.HomeFragment;
import com.jonathan.test.screens.home.viewmodel.provider.HomeProvider;
import com.jonathan.test.screens.mainhost.viewmodel.MainHostViewModel;
import com.jonathan.test.screens.mainhost.viewmodel.provider.MainHostProvider;
import com.jonathan.test.screens.result.ResultContract;
import com.jonathan.test.screens.result.view.ResultFragment;
import com.jonathan.test.screens.result.viewmodel.provider.ResultProvider;
import com.jonathan.test.util.ActivityUtils;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Activity responsible to host the fragments responsible for the bottom navigation bar tabs
 */
public class MainHostActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener,
        HomeContract.Host, ResultContract.Host {
    private MainHostViewModel mViewModel;

    private HomeFragment homeFragment;
    private ResultFragment resultFragment;
    private BottomNavigationViewEx bottomNavigationViewCustom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainhost);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mViewModel = ViewModelProviders.of(this, new MainHostProvider().new MainHost()).get("MainHostActivity:MainHostViewModel", MainHostViewModel.class);

        startComponents();

        mViewModel.messages().observe(this, this::onShowMessageRequested);
    }

    @Override
    public void startComponents() {
        myContext = this;

        //------------------ Attaching Input ------------------//
        homeFragment = (HomeFragment)getSupportFragmentManager().findFragmentByTag("main_host_home");
        if(homeFragment == null){
            homeFragment = new HomeFragment();
            ActivityUtils.attachFragmentToActivity(getSupportFragmentManager(), homeFragment, R.id.main_host_container, "main_host_home");
        }
        homeFragment.defineViewModelFactory(new HomeProvider().new Home(), "MainHostActivity:HomeViewModel");

        //------------------ Attaching Result ------------------//
        resultFragment = (ResultFragment)getSupportFragmentManager().findFragmentByTag("main_host_result");
        if(resultFragment == null){
            resultFragment = new ResultFragment();
            ActivityUtils.attachFragmentToActivity(getSupportFragmentManager(), resultFragment, R.id.main_host_container, "main_host_result");
        }
        resultFragment.defineViewModelFactory(new ResultProvider().new Result(), "MainHostActivity:ResultViewModel");

        //----------------------------------------------------//

        bottomNavigationViewCustom = findViewById(R.id.main_host_bottom_navigation_bar_custom);
        bottomNavigationViewCustom.setOnNavigationItemSelectedListener(this);
        bottomNavigationViewCustom.enableAnimation(false);
        bottomNavigationViewCustom.enableShiftingMode(false);
        bottomNavigationViewCustom.enableItemShiftingMode(false);

        selectMenuItem(R.id.menu_item_home);
    }

    @Override
    public void onBackPressed() {
        if(!homeFragment.isHidden() && homeFragment.isVisible()){
            super.onBackPressed();

        }else{
            selectMenuItem(R.id.menu_item_home);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        selectMenuItem(item.getItemId());
        return true;
    }


    /**
     * Deselect all menu items
     */
    private void deselectAllMenuItems(){
        Menu menu = bottomNavigationViewCustom.getMenu();
        menu.findItem(R.id.menu_item_home).setChecked(false);
        menu.findItem(R.id.menu_item_result).setChecked(false);
    }

    /**
     * Requests fragment tab to be selected
     * @param selectedFragment Fragment which tab should be selected
     */
    private void selectFragmentsTab(Fragment selectedFragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.test_transition_fade_in, R.anim.test_transition_fade_out);

        transaction.hide(homeFragment);
        transaction.hide(resultFragment);

        transaction.show(selectedFragment);
        transaction.commit();
    }

    /**
     * Requests menu item to be selected
     * @param menuItemResource Id of menu item to be selected
     */
    private void selectMenuItem(int menuItemResource) {
        deselectAllMenuItems();

        Menu menu = bottomNavigationViewCustom.getMenu();
        menu.findItem(menuItemResource).setChecked(true);

        switch (menuItemResource){
            case R.id.menu_item_home:
                selectFragmentsTab(homeFragment);

                break;
            case R.id.menu_item_result:
                selectFragmentsTab(resultFragment);

                break;
        }
    }

    /******************** COMMON HOST CONTRACT ********************/


    @Override
    public void selectNavigationBarIcon(int menuItemId) {
        MenuItem menuItem = bottomNavigationViewCustom.getMenu().findItem(menuItemId);

        menuItem.setChecked(true);
    }

    @Override
    public void closeScreen() {
        finish();
    }


    /******************** HOME HOST CONTRACT ********************/

    @Override
    public void showFilteredCustomers(List<ECustomer> customers) {
        resultFragment.onNewSearchPerformed(customers);
        selectMenuItem(R.id.menu_item_result);
    }
}