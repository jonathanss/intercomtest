package com.jonathan.test.screens.home.view;

import com.jonathan.test.R;
import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.BaseFragment;
import com.jonathan.test.screens.home.HomeContract;
import com.jonathan.test.screens.home.viewmodel.HomeViewModel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Fragment view implementation of Home Screen
 */
public class HomeFragment extends BaseFragment<HomeContract.ViewModel, HomeContract.Host> implements
        View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private Toolbar homeToolbar;

    private TextView txtSelectedDistance;
    private TextView txtMinimumDistance;
    private TextView txtMaximumDistance;
    private TextView txtSourceFileName;
    private Button btnSearch;
    private SeekBar seekDistancePicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel.getFilteredCustomers().observe(this, this::onFilteredCustomersChanged);
        mViewModel.getDefaultSelectedDistance().observe(this, this::onDefaultDistanceChanged);
        mViewModel.getDefaultMinimumDistance().observe(this, this::onMinimumDistanceChanged);
        mViewModel.getDefaultMaximumDistance().observe(this, this::onMaximumDistanceChanged);
        mViewModel.getSourceFileName().observe(this, this::onSourceFileNameChanged);
        mViewModel.getSearchButtonStatus().observe(this, this::onSearchButtonStatusChanged);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        homeToolbar = view.findViewById(R.id.home_fragment_toolbar);

        ((AppCompatActivity)requireActivity()).setSupportActionBar(homeToolbar);
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtSelectedDistance = view.findViewById(R.id.home_fragment_selected_distance);
        txtMinimumDistance = view.findViewById(R.id.home_fragment_minimum_distance);
        txtMaximumDistance = view.findViewById(R.id.home_fragment_maximum_distance);
        txtSourceFileName = view.findViewById(R.id.home_fragment_file_name);
        seekDistancePicker = view.findViewById(R.id.home_fragment_distance);
        btnSearch = view.findViewById(R.id.home_fragment_button_search);

        seekDistancePicker.setOnSeekBarChangeListener(this);
        btnSearch.setOnClickListener(this);

        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if(!hidden && mHost != null){
            mHost.selectNavigationBarIcon(R.id.menu_item_home);

            ((AppCompatActivity)requireActivity()).setSupportActionBar(homeToolbar);
            ((AppCompatActivity)requireActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    protected void initViewModel() {
        mViewModel = ViewModelProviders.of(this, mFactory).get(viewModelKey, HomeViewModel.class);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.home_fragment_button_search){
            mViewModel.handleSearchButtonClick(seekDistancePicker.getProgress());
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        txtSelectedDistance.setText(getString(R.string.home_label_distance, progress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) { }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) { }



    /******************** LIVE DATA OBSERVERS ********************/

    private void onFilteredCustomersChanged(List<ECustomer> customers){
        mHost.showFilteredCustomers(customers);
    }

    private void onDefaultDistanceChanged(int distance){
        seekDistancePicker.setProgress(distance, true);
    }

    private void onMinimumDistanceChanged(int distance){
        seekDistancePicker.setMin(distance);
        txtMinimumDistance.setText(getString(R.string.home_label_distance, distance));
    }

    private void onMaximumDistanceChanged(int distance){
        seekDistancePicker.setMax(distance);
        txtMaximumDistance.setText(getString(R.string.home_label_distance, distance));
    }

    private void onSourceFileNameChanged(String fileName){
        txtSourceFileName.setText(fileName);
    }

    private void onSearchButtonStatusChanged(boolean enabled){
        btnSearch.setEnabled(enabled);
    }
}
