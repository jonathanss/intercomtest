package com.jonathan.test.usecase.exception;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Custom Exception used by UseCases within the app
 */
public class ReadFileException extends Exception {
    private int userFriendlyMessageId;

    /**
     * ReadFileException constructor
     * @param userFriendlyMessageId Id of user friendly message to be shown for this exception
     */
    public ReadFileException(int userFriendlyMessageId){
        this(userFriendlyMessageId, null, null);
    }

    /**
     * ReadFileException constructor
     * @param userFriendlyMessageId Id of user friendly message to be shown for this exception
     * @param message Original exception message to be logged
     */
    public ReadFileException(int userFriendlyMessageId, String message){
        this(userFriendlyMessageId, message, null);
    }

    /**
     * ReadFileException constructor
     * @param userFriendlyMessageId Id of user friendly message to be shown for this exception
     * @param message  Original exception message to be logged
     * @param cause Original exception which stacktrace will be logged from
     */
    public ReadFileException(int userFriendlyMessageId, String message, Throwable cause){
        super(message, cause);

        this.userFriendlyMessageId = userFriendlyMessageId;
    }

    /**
     * Requests resource id of user friendly message to be shown for this exception
     * @return resource id of user friendly message to be shown for this exception
     */
    public int getUserFriendlyMessageId(){
        return userFriendlyMessageId;
    }
}
