package com.jonathan.test.usecase;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;

import com.jonathan.test.R;
import com.jonathan.test.usecase.exception.ReadFileException;
import com.jonathan.test.util.ConstUtil;
import com.jonathan.test.util.FunctionUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Use Case responsible for retrieving list of Customers
 */
public class RetrieveCustomersUseCase {

    /**
     * Request list of customers available on internal file
     * @return list of customers as JsonObject list
     * @throws ReadFileException Thrown if any error occurs during file reading
     */
    public List<JsonObject> readFromInternalFile() throws ReadFileException{
        validateFileName(ConstUtil.def.DEFAULT_SOURCE_FILE_NAME);

        List<JsonObject> result = new ArrayList<>();

        try(InputStream inputStream = Objects.requireNonNull(this.getClass().getClassLoader())
                .getResourceAsStream("res/raw/" + ConstUtil.def.DEFAULT_SOURCE_FILE_NAME);
            InputStreamReader reader = new InputStreamReader(inputStream)){
            JsonStreamParser parser = new JsonStreamParser(reader);

            while (parser.hasNext()){
                JsonElement element = parser.next();

                if(element.isJsonObject()){
                    result.add(element.getAsJsonObject());
                }
            }

            return result;

        }catch (FileNotFoundException fileNotFoundException){
            throw new ReadFileException(R.string.error_file_not_found, fileNotFoundException.getMessage(), fileNotFoundException);

        }catch (NullPointerException nullPointerException){
            throw new ReadFileException(R.string.error_file_not_found, nullPointerException.getMessage(), nullPointerException);

        }catch (IOException ioException){
            throw new ReadFileException(R.string.error_reading_file, ioException.getMessage(), ioException);
        }
    }

    /**
     * Requests file name to be validate
     * @param fileName File name to be validate
     * @throws ReadFileException Thrown if any error occurs during file name validation or if file name is not valid
     */
    public void validateFileName(String fileName) throws ReadFileException {
        if(FunctionUtil.isEmpty(fileName)){
            throw new ReadFileException(R.string.error_invalid_file_name);
        }

        if(!fileName.contains(".")){
            throw new ReadFileException(R.string.error_invalid_file_name, "Missing extension on file name.");
        }

        if(!fileName.matches("^[\\w\\-. ]+$")){
            throw new ReadFileException(R.string.error_invalid_file_name, "Rejected by regular expression");
        }
    }
}
