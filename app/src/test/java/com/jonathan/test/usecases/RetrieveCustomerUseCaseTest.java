package com.jonathan.test.usecases;

import com.jonathan.test.R;
import com.jonathan.test.usecase.RetrieveCustomersUseCase;
import com.jonathan.test.usecase.exception.ReadFileException;

import org.junit.Assert;
import org.junit.Test;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Responsible for execution of RetrieveCustomersUseCase tests
 */
public class RetrieveCustomerUseCaseTest {
    private RetrieveCustomersUseCase mUseCase;

    /**
     * RetrieveCustomerUseCaseTest constructor
     */
    public RetrieveCustomerUseCaseTest(){
        mUseCase = new RetrieveCustomersUseCase();
    }

    @Test
    public void validateNullFileName(){
        try{
            mUseCase.validateFileName(null);

        } catch (ReadFileException error) {
            Assert.assertNotNull(error);
            Assert.assertNotEquals(0, error.getUserFriendlyMessageId());
            Assert.assertEquals(R.string.error_invalid_file_name, error.getUserFriendlyMessageId());
        }
    }

    @Test
    public void validateEmptyFileName(){
        try{
            mUseCase.validateFileName("");

        } catch (ReadFileException error) {
            Assert.assertNotNull(error);
            Assert.assertNotEquals(0, error.getUserFriendlyMessageId());
            Assert.assertEquals(R.string.error_invalid_file_name, error.getUserFriendlyMessageId());
        }
    }

    @Test
    public void validateNoExtensionFileName(){
        try{
            mUseCase.validateFileName("test");

        } catch (ReadFileException error) {
            Assert.assertNotNull(error);
            Assert.assertNotEquals(0, error.getUserFriendlyMessageId());
            Assert.assertEquals(R.string.error_invalid_file_name, error.getUserFriendlyMessageId());
        }
    }

    @Test
    public void validateNotAcceptableFileName(){
        try{
            mUseCase.validateFileName(".txt");

        } catch (ReadFileException error) {
            Assert.assertNotNull(error);
            Assert.assertNotEquals(0, error.getUserFriendlyMessageId());
            Assert.assertEquals(R.string.error_invalid_file_name, error.getUserFriendlyMessageId());
        }
    }
}
