package com.jonathan.test;

import com.jonathan.test.interactors.HomeInteractorTest;
import com.jonathan.test.interactors.ResultInteractorTest;
import com.jonathan.test.usecases.RetrieveCustomerUseCaseTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sun 22 of Feb, 2020
 *
 * Description:
 * Responsible for executing a suite with all available Unit Tests
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({HomeInteractorTest.class, ResultInteractorTest.class, RetrieveCustomerUseCaseTest.class})
public class Main { }
