package com.jonathan.test.interactors;

import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.result.viewmodel.ResultInteractor;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Responsible for execution of ResultInteractor tests
 */
public class ResultInteractorTest {
    private ResultInteractor mInteractor;

    /**
     * ResultInteractorTest constructor
     */
    public ResultInteractorTest(){
        mInteractor = new ResultInteractor();
    }

    @Test
    public void sortNullList(){
        mInteractor.sortCustomers(null);

        Assert.assertTrue(true); //Assuming the test case passed as no exception was thrown
    }

    @Test
    public void sortEmptyList(){
        List<ECustomer> input = new ArrayList<>();

        mInteractor.sortCustomers(input);

        Assert.assertTrue(input.isEmpty());
    }

    @Test
    public void sortCheckListSize(){
        List<ECustomer> input = new ArrayList<>();

        for(int index = 9; index >= 0; index--){
            ECustomer customer = new ECustomer();

            customer.setId(index+1);
            customer.setName("Customer" + (index+1));
            customer.setLatitude(index+1);
            customer.setLongitude(index+1);

            input.add(customer);
        }

        mInteractor.sortCustomers(input);

        Assert.assertNotNull(input);
        Assert.assertEquals(10, input.size());
    }

    @Test
    public void sortCheckListContent(){
        List<ECustomer> input = new ArrayList<>();

        for(int index = 9; index >= 0; index--){
            ECustomer customer = new ECustomer();

            customer.setId(index+1);
            customer.setName("Customer" + (index+1));
            customer.setLatitude(index+1);
            customer.setLongitude(index+1);

            input.add(customer);
        }

        mInteractor.sortCustomers(input);

        Assert.assertNotNull(input);
        Assert.assertEquals(10, input.size());

        for(int index = 0; index < 9; index++){
            Assert.assertEquals(input.get(index).getId(), index+1);
        }
    }

    @Test
    public void getFriendlyNullDate(){
        String representation = mInteractor.getUserFriendlyDate(null);

        Assert.assertNotNull(representation);
        Assert.assertFalse(representation.isEmpty());
    }
}
