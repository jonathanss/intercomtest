package com.jonathan.test.interactors;

import com.google.gson.JsonObject;

import com.jonathan.test.model.entity.ECustomer;
import com.jonathan.test.screens.home.viewmodel.HomeInteractor;
import com.jonathan.test.util.ConstUtil;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * intercomtest
 *
 * Created by jonathan
 * Creation date Sat 22 of Feb, 2020
 *
 * Description:
 * Responsible for execution of HomeInteractor tests
 */
public class HomeInteractorTest {
    private HomeInteractor mInteractor;

    /**
     * HomeInteractorTest constructor
     */
    public HomeInteractorTest(){
        mInteractor = new HomeInteractor();
    }

    @Test
    public void parseNullList(){
        List<ECustomer> result = mInteractor.parse(null);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void parseEmptyList(){
        List<ECustomer> result = mInteractor.parse(new ArrayList<>());

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void parseWrongFormatList(){
        List<JsonObject> input = new ArrayList<>();

        for(int index = 0; index < 10; index++){
            JsonObject obj = new JsonObject();

            obj.addProperty("propertyTest", "valueTest");
            input.add(obj);
        }

        List<ECustomer> result = mInteractor.parse(input);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void parseCheckListSize(){
        List<JsonObject> input = new ArrayList<>();

        for(int index = 0; index < 10; index++){
            JsonObject obj = new JsonObject();

            obj.addProperty("user_id", index+1);
            obj.addProperty("name", "name" + index+1);
            obj.addProperty("latitude", index+1);
            obj.addProperty("longitude", index+1);
            input.add(obj);
        }

        List<ECustomer> result = mInteractor.parse(input);

        Assert.assertNotNull(result);
        Assert.assertEquals(10, result.size());
    }

    @Test
    public void parseCheckListContent(){
        List<JsonObject> input = new ArrayList<>();

        for(int index = 0; index < 10; index++){
            JsonObject obj = new JsonObject();

            obj.addProperty("user_id", index+1);
            obj.addProperty("name", "name" + index+1);
            obj.addProperty("latitude", index+1);
            obj.addProperty("longitude", index+1);
            input.add(obj);
        }

        List<ECustomer> result = mInteractor.parse(input);

        Assert.assertNotNull(result);
        Assert.assertEquals(10, result.size());

        for(int index = 0; index < 10; index++){
            ECustomer customer = result.get(index);

            Assert.assertNotNull(customer);

            Assert.assertEquals(index+1, customer.getId());
            Assert.assertEquals("name" + index+1, customer.getName());
            Assert.assertEquals(index+1, customer.getLatitude(), 0.001);
            Assert.assertEquals(index+1, customer.getLongitude(), 0.001);
        }
    }

    @Test
    public void filterNullList(){
        List<ECustomer> result = mInteractor.filter(null, 100);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void filterEmptyList(){
        List<ECustomer> result = mInteractor.filter(new ArrayList<>(), 100);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void filterWrongFormatList(){
        List<ECustomer> input = new ArrayList<>();

        for(int index = 0; index < 10; index++){
            ECustomer customer = new ECustomer();

            customer.setId(index); //invalid id
            customer.setName("Valid Name");
            customer.setLatitude(index); // invalid latitude
            customer.setLongitude(index+1);

            input.add(customer);
        }

        List<ECustomer> result = mInteractor.filter(input, 100);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void filterCheckListSize(){
        List<ECustomer> input = new ArrayList<>();

        for(int index = 0; index < 10; index++){
            ECustomer customer = new ECustomer();

            customer.setId(index+1);
            customer.setName("Valid Name");
            customer.setLatitude(ConstUtil.def.DUBLIN_OFFICE_LAT);
            customer.setLongitude(ConstUtil.def.DUBLIN_OFFICE_LONG);

            input.add(customer);
        }

        List<ECustomer> result = mInteractor.filter(input, 1);

        Assert.assertNotNull(result);
        Assert.assertEquals(10, result.size());
    }

    @Test
    public void filterCheckListContent(){
        List<ECustomer> input = new ArrayList<>();

        ECustomer customer1 = new ECustomer();
        customer1.setId(1);
        customer1.setName("Customer 1");
        customer1.setLatitude(ConstUtil.def.DUBLIN_OFFICE_LAT);
        customer1.setLongitude(ConstUtil.def.DUBLIN_OFFICE_LONG);
        input.add(customer1);

        ECustomer customer2 = new ECustomer();
        customer2.setId(2);
        customer2.setName("Customer 2");
        customer2.setLatitude(53.239746); //Customer is 11.11km away
        customer2.setLongitude(-6.245265); //Customer is 11.11km away
        input.add(customer2);

        ECustomer customer3 = new ECustomer();
        customer3.setId(3);
        customer3.setName("Customer 3");
        customer3.setLatitude(53.2451022); //Customer is 10.57km away
        customer3.setLongitude(-6.238335); //Customer is 10.57km away
        input.add(customer3);

        ECustomer customer4 = new ECustomer();
        customer4.setId(4);
        customer4.setName("Customer 4");
        customer4.setLatitude(53.008769); //Customer is 38.14km away
        customer4.setLongitude(-6.1056711); //Customer is 38.14km away
        input.add(customer4);


        List<ECustomer> result = mInteractor.filter(input, 11);

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(1, result.get(0).getId());
        Assert.assertEquals(3, result.get(1).getId());
    }
}
